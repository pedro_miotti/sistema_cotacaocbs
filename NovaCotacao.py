from tkinter import *
from tkinter import ttk
from tkinter import messagebox

from datetime import date

from fpdf import FPDF

import os


cotacao_win = Tk()
cotacao_win.geometry("800x620")
cotacao_win.title("Simular Cotacao")

#Creating Icon
cotacao_win.iconbitmap('Imagens\cbsicon.ico')

#TREEVIEW----------
tree_produtos = ttk.Treeview(cotacao_win, show='headings')
tree_produtos["columns"] = ("one", "two", "three", "four", "five")

tree_produtos.column("one", width=50)
tree_produtos.column("two", width=400)
tree_produtos.column("three", width=150)
tree_produtos.column("four", width=92)
tree_produtos.column("five", width=92)

tree_produtos.heading("one", text='Qtd.', anchor='w')
tree_produtos.heading("two", text="Produto", anchor='w')
tree_produtos.heading("three", text="Marca", anchor='w')
tree_produtos.heading("four", text="Preco Unitario", anchor='w')
tree_produtos.heading("five", text="Preco Total", anchor='w')

tree_produtos.place(x = 5, y = 190)


#LABELS-----
#textvariables
empresa_str = StringVar()
telefone_str = StringVar()
contato_str = StringVar()
email_str = StringVar()
qtdcestas_txt = IntVar()

empresaL = Label(cotacao_win, font=("Courier new", 13), text="Empresa :")
empresaL.place(x= 0, y = 10)

telefoneL = Label(cotacao_win, font=("Courier new", 13), text="Telefone :")
telefoneL.place(x= 0, y = 47)

contatoL = Label(cotacao_win, font=("Courier new", 13), text="Contato :")
contatoL.place(x = 400, y = 10)

emailL = Label(cotacao_win,font=("Courier new", 13), text="E-mailL :")
emailL.place(x = 280 , y = 47)

qtd_cestasL = Label(cotacao_win,font=("Courier new", 12), text="Qtd.\nCestas :")
qtd_cestasL.place(x =615, y = 37)



#ENTRYS-----
empresaE = Entry(cotacao_win, width = 25, font=("arial", 14), relief = "flat", textvariable = empresa_str)
empresaE.place(x = 100, y = 10)

telefoneE = Entry(cotacao_win, width = 13, font=("arial", 14), relief = "flat", textvariable = telefone_str)
telefoneE.place(x = 115, y = 47)

contatoE = Entry(cotacao_win, width = 13, font=("arial", 14), relief = "flat", textvariable = contato_str)
contatoE.place(x = 500, y = 10)

emailE = Entry(cotacao_win, width = 23, font=("arial", 13), relief = "flat", textvariable = email_str)
emailE.place(x = 380, y = 48)

qtd_cestasE = Entry(cotacao_win, width =6, font=("arial", 13), relief = "flat", textvariable = qtdcestas_txt)
qtd_cestasE.place(x = 710, y = 53)

#TEXTFRAME----
lblframe = LabelFrame(cotacao_win , font=("courier new",12), text = "Add produto", width =789, height = 100)
lblframe.place(x = 5 , y= 80)

produtoL = Label(lblframe, font=("Courier new", 13), text="Produto :")
produtoL.place(x = 3, y = 15)

qtdL = Label(lblframe, font=("Courier new", 13), text="Quantidade :")
qtdL.place(x = 380, y = 45)

precoL = Label(lblframe, font=("Courier new", 13), text="Preco :")
precoL.place(x = 21, y = 45)

marca = Label(lblframe, font=("Courier new", 13), text="Marca :")
marca.place(x = 380, y = 15)



#ENTRY---------
#textvariables
add_prod_strvar = StringVar()
preco_floatvar = DoubleVar()
marca_strvar = StringVar()

add_prod = Entry(lblframe ,font=('Courier new', 13), width = 25, relief = "flat", textvariable = add_prod_strvar)
add_prod.place(x=109, y= 15)

precoE = Entry(lblframe ,font=('Courier new', 13), width = 10, relief = "flat", textvariable = preco_floatvar)
precoE.place(x=109, y= 45)

marcaE = Entry(lblframe ,font=('Courier new', 13), width = 14, relief = "flat", textvariable = marca_strvar)
marcaE.place(x= 465,y = 17)

#SPINBOX------
qtd_intvar = IntVar()
qtdS = Spinbox(lblframe, from_=0, to=1000, width =5, relief = "flat", font=("courier new", 12), textvariable = qtd_intvar)
qtd_intvar.set(1)
qtdS.place(x=515, y= 45)

#DEFINING FUNTCTIONS--------------
def inserir():
    preco_get = preco_floatvar.get()
    add_prod_get = add_prod_strvar.get()
    qtd_get = qtd_intvar.get()
    marca_get = marca_strvar.get()

    preco_total = qtd_get * preco_get
    preco_round = round(preco_total, 2)

    tree_produtos.insert("" , END, values=(qtd_get, add_prod_get, marca_get ,preco_get, preco_round))

    #preco_floatvar.set(0.0)
    #add_prod.delete(0, END)
    #qtd_intvar.set(1)
    #marcaE.delete(0, END)

def deletar():
    selected_item = tree_produtos.selection()
    tree_produtos.delete(selected_item)

def deletar_tudo():
    msg = messagebox.askquestion("!", "Tem certeza que deseja deletar tudo ?",icon = 'warning')

    if msg == 'yes':
       tree_produtos.delete(*tree_produtos.get_children())
    else:
        pass



#BUTTON-----------
add_btt = Button(lblframe, text = "Adicionar", font=("courier new", 13), height =2, command = inserir)
add_btt.place(x = 640, y = 5)

deletar_btt = Button(cotacao_win, text = "Deletar", font=("courier new", 13), height =2, command = deletar )
deletar_btt.place(x = 5, y = 420 )

deletar_tudo_btt = Button(cotacao_win, text = "Deletar\nTudo", font=("courier new", 13), height =2,command = deletar_tudo )
deletar_tudo_btt.place(x = 90, y = 420 )

#TEXTFRAME---------------
resultados_frame = LabelFrame(cotacao_win , font=("courier new",12), text = "Dados do Orcamento", width =789, height = 130)
resultados_frame.place(x = 5 , y= 485)

#textvariables
totalitens_txt = StringVar()
valorunitario_txt = StringVar()
valortotal_txt = DoubleVar()
#LABELS---------------
#TotalItens, ValorUnitario, ValorTotal, ValidadeDeProposta, Entrega, DadosDoVendedor(Nome), Telefone, NumeroDoPat

#1 Coluna======
#TotalItens
totalItens = Label(resultados_frame, font=("Arial", 13), text = "Total Itens :")
totalItens.place(x = 0, y = 10)
totalItens_show = Label(resultados_frame, font=("Courier", 13), text = "", textvariable = totalitens_txt)
totalitens_txt.set("....." + "0")
totalItens_show.place(x = 85, y = 10)

#ValorUnitario
ValorUnitario = Label(resultados_frame, font=("Arial", 13), text = "Valor Unitario :")
ValorUnitario.place(x = 0, y = 40)
ValorUnitario_show = Label(resultados_frame, font=("Courier", 13), text = "R$100", textvariable = valorunitario_txt )
valorunitario_txt.set("....." + "R$0")
ValorUnitario_show.place(x = 110, y = 40)

#ValorTotal
valorTotal = Label(resultados_frame, font=("Arial", 13), text = "Valor Total :" )
valorTotal.place(x = 0, y = 70)
valorTotal_show = Label(resultados_frame, font=("Courier", 13) , textvariable = valortotal_txt)
valortotal_txt.set("....." + "R$0")
valorTotal_show.place(x = 89, y = 70)

#2 Coluna======
vendedor = Label(resultados_frame, font=("Arial", 13), text = "Vendedor :" )
vendedor.place(x = 250, y = 8)

#ADDING TO THE COMBOBOX
def vendedores_cb():

    vendedores_file = open("Texto\\vendedores.txt","r").readlines()
    vendedores = []

    for line in vendedores_file:
        vendedores.append(line)

    return vendedores

vendedor_str = StringVar()
vendedor_box = ttk.Combobox(resultados_frame, font=("Courier", 13), textvariable = vendedor_str )
vendedor_box.place(x = 340, y = 8)

vendedor_box['values'] = vendedores_cb()

#Funcao atualizar
def atualizar():
    global itenstotal
    #total de itens
    itenstotal = 0

    for child in tree_produtos.get_children():
        itenstotal += int(tree_produtos.item(child, "values")[0])

        totalitens_txt.set("....." + format(itenstotal))

    #Valor Unitario
    unitariovalor = 0.0

    for child in tree_produtos.get_children():
        unitariovalor += float(tree_produtos.item(child, "values")[4])

        valorunitario_txt.set("....." + "R$" + format(unitariovalor))


    #Valor Total
    totalvalor = 0.0
    cestas_get = qtdcestas_txt.get()

    totalvalor += unitariovalor * cestas_get

    valortotal_txt.set("....." + "R$" + format(totalvalor))


def gerarpdf(spacing=2):
    #Header Variables
    empresa_get = empresa_str.get()
    telefone_get = telefone_str.get()
    contato_get = contato_str.get()
    email_get = email_str.get()

    cidade = "Sorocaba , "
    protect_bag = "AQUI TEM QUALIDADE"
    protect_bag2 = "TABALHAMOS COM PROTECT BAG"

    #DataDeHoje
    hoje = date.today()
    data = hoje.strftime('%d/%m/%y')

    #Footer Variables
    totalitens_pdf = totalitens_txt.get()
    valorUnitario_pdf = valorunitario_txt.get()
    vendedor_str_get = vendedor_str.get()

    pdf = FPDF()
    pdf.set_top_margin(88)
    pdf.add_page()

#HEADER ------------------
    #cbscestaslogo
    pdf.image('Imagens\Logo-Cbs.jpg',5,5,50,25)

    #lineTop&Bottom
    pdf.line(4,5,205,5)
    pdf.line(4,70,205,70)
    #lineSides
    pdf.line(205,70,205,5)
    pdf.line(4,70,4,5)

    #A
    pdf.set_font('Arial', 'B', 13)
    pdf.text(10, 40, "Á ")
    #nome empresa
    pdf.set_font('Arial', '', 12)
    pdf.text(17, 40, empresa_get)

    #A/C
    pdf.set_font('Arial', 'B', 13)
    pdf.text(10, 48, "A/C :")
    #Contato
    pdf.set_font('Arial', '', 12)
    pdf.text(22, 48, contato_get)

    #Fone :
    pdf.set_font('Arial', 'B', 13)
    pdf.text(10, 56, "Fone :")
    #Fone
    pdf.set_font('Arial', '', 12)
    pdf.text(26, 56, telefone_get)

    #Email :
    pdf.set_font('Arial', 'B', 13)
    pdf.text(10, 64, "E-mail :")
    #Email
    pdf.set_font('Arial', '', 12)
    pdf.text(28, 64, email_get)

    #Protect Bag
    pdf.set_font('Arial', 'B', 11)
    pdf.text(130, 52, protect_bag)
    pdf.set_font('Arial', 'B', 11)
    pdf.text(120, 58, protect_bag2)

    #Data
    pdf.set_font('Arial', '', 14)
    pdf.text(130, 40, cidade + data)

#COPOSICAO -------------------
    #Qtd
    pdf.set_font('Arial', 'B', 13)
    pdf.text(10, 78, "QTD.")

    #produto
    pdf.set_font('Arial', 'B', 13)
    pdf.text(40, 78, "PRODUTO")

    #Marca
    pdf.set_font('Arial', 'B', 13)
    pdf.text(150, 78, "MARCA")

    pdf.set_font("Arial", size=11)

    for child in tree_produtos.get_children():
        col_width = pdf.w / 4.5
        row_height = pdf.font_size

        pdf.cell(col_width, 0, tree_produtos.item(child, 'values')[0], 0, 1, "l")
        pdf.ln(0)

        pdf.cell(30)
        pdf.cell(col_width,0, tree_produtos.item(child, 'values')[1],0, 1, "l")
        pdf.ln(0)

        pdf.cell(140)
        pdf.cell(col_width, 0, tree_produtos.item(child, 'values')[2], 0, 1, "l")
        pdf.ln(row_height*spacing)

#FOOTER ------------------

    #lineTop&Bottom
    pdf.line(4,250,205,250)
    pdf.line(4,290,205,290)
    #lineSides
    pdf.line(205,250,205,290)
    pdf.line(4,250,4,290)

#1 coluna
    #Total de itens
    pdf.set_font('Arial', 'B', 12)
    pdf.text(10, 260, "Total itens ")

    pdf.set_font('Arial', '', 11)
    pdf.text(31, 260, totalitens_pdf)

    #Valor Unitario
    pdf.set_font('Arial', 'B', 12)
    pdf.text(10, 270, "Valor Unitario ")

    pdf.set_font('Arial', '', 11)
    pdf.text(38, 270, valorUnitario_pdf)

    #Numero Pat
    pdf.set_font('Arial', 'B', 12)
    pdf.text(10, 280, "Vendedor ")

    pdf.set_font('Arial', '', 11)
    pdf.text(29, 280, "....." + vendedor_str_get)

    #2 Coluna===============
    #Validade De Proposta
    pdf.set_font('Arial', 'B', 12)
    pdf.text(70, 260, "Validade da Proposta ")

    pdf.set_font('Arial', '', 11)
    pdf.text(114, 260, ".....03 Dias")

    #Numero Pat
    pdf.set_font('Arial', 'B', 12)
    pdf.text(70, 270, "Numero do PAT ")

    pdf.set_font('Arial', '', 11)
    pdf.text(102, 270, ".....080146094")

    #Numero Telefone
    pdf.set_font('Arial', 'B', 12)
    pdf.text(70, 280, "Telefone ")

    pdf.set_font('Arial', '', 11)
    pdf.text(88, 280, ".....(15)3033-8668")

    pdf.output(empresa_get + '.pdf', 'F')

    messagebox.showinfo(':D', "PDF Gerado com sucesso ", parent= gerar_pdf)

    os.startfile(empresa_get + ".pdf")





#Botao PDF
gerar_pdf = Button(cotacao_win, text = "Gerar PDF", font=("courier new", 13), height =2 , command = gerarpdf)
gerar_pdf.place(x = 687, y = 420 )

#Botao Atualizar
AtualizarBtn = Button(text = "Atualizar", font=("courier new", 13), height =2, command = atualizar )
AtualizarBtn.place(x = 175, y = 420)





cotacao_win.mainloop()
